require 'factory_girl_rails'

# 200.times do
#   b = FactoryGirl.create(:booking, :four_days_from_now)
#   b.unassign_provider!
# end

# booking = FactoryGirl.create(:booking, :two_days_from_now)
# provider = booking.provider
# booking.unassign_provider!
provider = Provider.last
dates = [(Time.now + 1.day).beginning_of_day]
jobs_details = nil
## "baking" our config params so we don't have to refetch
context = API::Portal::V3::Models::JobDetails::Context::Claimable.new(provider: provider)
context = API::Portal::V3::Models::JobDetails::Context::Geospatial.new(context)
jobs_by_date = API::Portal::V3::Models::JobDetails::Factory.new(dates: dates, context: context).build
jobs_details = API::Portal::V3::Presenters::JobsDetailsPresenter.new(jobs_by_date: jobs_by_date, context: context)
jobs_details.to_json

StackProf.run(mode: :wall, interval: 500, out: "tmp/stackprof-wall-500ms-100-available-jobs-no-waive.dump", raw: true) do
  context = API::Portal::V3::Models::JobDetails::Context::Claimable.new(provider: provider)
  #### decorate context
  context = API::Portal::V3::Models::JobDetails::Context::Geospatial.new(context)
  jobs_by_date = API::Portal::V3::Models::JobDetails::Factory.new(dates: dates, context: context).build
  jobs_details = API::Portal::V3::Presenters::JobsDetailsPresenter.new(jobs_by_date: jobs_by_date, context: context)
  jobs_details.to_json
end
