### Gathering of the n00bs 1.0

We met on 10/9/19 to talk about our onboarding experience. There were seven of us who'd 
worked at Handy for between **2 weeks** and **2 months**

Avg professional programming experience: (3 + 5 + 3 + 7 + 5 + 6 + 5)/7 = **4.85 years**

**5** of us had worked with microservices before, **2** hadn't

**1** of us had programmed in Rails professionally, **6** of us hadn't

**5** of us use RubyMines, **1** of us uses VSCode, **1** of us uses Sublime

#### Prompting questions

During onboarding, what ... 

* went well
* was confusing
* went poorly/made us feel bad

#### Themes that arose (tl;dr)

* Handy eng is great at answering questions and making us feel comfortable asking questions
* Documentation is hard to find, potentially outdated, and potentially lacking
* Fast frictionless deploys are empowering but can feel risky without knowledge on how to monitor your changes
* Handybook is tough to work in sometimes

#### Action Items (tl;dr++)

* Gather all of the documentation/learning/tips we've found. Post it in `#n00bs`
* compile our docs and figure out where the gaps in the current docs are, and where docs should live
* Make the linter run locally, and not spam Gitlab MRs
* Share notes with eng leadership, especially around first-day push expectation setting

#### The meeting

After writing our answers to the prompting questions, we started a somewhat structured 
discussion about our experiences so far at Handy.

##### Pushing on the first day

The first thing that came up was a general sense that there was too much information to fully 
process on the first few days. Generally, we felt like it was unavoidable that we felt a little 
overwhelmed. But we also felt like there was a lot of pressure to push on our first day. Without 
an understanding of why that pressure was there, the firehose of information felt a little more 
overwhelming.

All of us felt the pressure to push on the first day, and the reaction to that pressure was 
both positive and negative. We liked the fact that we can and are expected to push often and 
with as gatekeeping as possible. But we would have liked to have had the reasoning behind the 
"push" to push on the first day explained to us up front.

We all had a buddy assigned, but some of them were too busy on our first day, and couldn't help 
us get our code out the door.

##### Deploys

Mixed feelings about the fact that you're solely responsible for QA and monitoring of your change.

We like the empowerment, but there was a general lack of confidence about how to QA our change 
after we deploy. When do we hit the "All Clear" button?

Only one of us was using New Relic to monitor their changes post-deploy.

##### Getting set up

Getting signed up for everything you needed to sign up for, your passwords in order, and going 
through emails took up a large chunk of time on the first day.

Lack of Docker knowledge makes understanding `hdy` commands much more difficult. It's hard to 
know what the `hdy` tool can do without looking at the source code, and even then it's a little 
opaque. The `hdy` command line help is out of date/missing commands.

Setting up and starting some microservices could be difficult and poorly documented. It's hard 
to be given a programming task, and spend all day figuring out how to get your local development 
environment running.

##### Asking questions

Generally we felt that, during onboarding, we were able to ask as many questions as 
needed. We didn't feel like a nuisance asking questions or using the `@here` alert in slack. 
We felt comfortable getting the information we needed.

The concentration of most dev knowledge in only 3 channels: `#dev`, `local-dev`, and `devops` 
made it easy to know where to ask questions, especially about general setup and environment issues

##### Finding specific information

While asking questions was easy, finding information for yourself was difficult. Documentation 
is lacking or outdated, sometimes by years. A lot of knowledge is just maintained in our heads. 
As mentioned above, testing and spinup steps often lack documentation, errors sometimes aren't 
descriptive, and we all come to Handy with varying degrees (lack) of experience with containerized 
microservices in Rails.

It's difficult sometimes to disentangle the source of errors/issues. Is it something with docker, 
rails configuration, ruby, the `hdy` command...?

##### Other issues

Handybook was consistently listed as a painpoint. It takes a long time to get running, you can't 
run unittests locally, and the impression is that the code is of a lower quality than other 
codebases at handy. Interesting counter-example to what we're learning in POODR.

Pushing to a handybook branch, then having to wait 20-ish minutes for tests to run was frustrating.

We didn't know/couldn't think of what service might be considered an "exemplary" service that 
we could look to for coding examples.

The Linter was generally appreciated, but the inability to run it locally was frustrating, as 
is the format its comments take on GitLab.