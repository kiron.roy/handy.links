### Pricing Impacting Q&A

#### Local Testing

We'll be using a Postman workspace to directly test our APIs locally and in namespaces

**FOR LOCAL TESTING ONLY**  DO THE FOLLOWING TO TURN OFF AUTH **FOR LOCAL TESTING ONLY**

1. Copy `turn_off_auth.patch` to your handybook root
2. run `git apply turn_off_auth.patch`

You'll need the following services:

* [handybook](https://gitlab.shared.handy-internal.com/engineering/developer-toolkit/blob/master/doc/monolith.md)
* [pricing](https://gitlab.shared.handy-internal.com/engineering/developer-toolkit/blob/master/doc/services/service-pricing-backend.md)

Then you should be able to run the invoice and booking creation postman requests against your local env

