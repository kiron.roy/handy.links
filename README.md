### Links to get you started at Handy

Once you've run through your emails and done all your first-day stuff, this list of links/tips/notes might help you get up to speed

#### Env setup

Run through a normal ssh key setup and add them to [gitlab](https://gitlab.shared.handy-internal.com/profile/keys) and [github](https://github.com/settings/keys)

[Developer Toolkit](https://gitlab.shared.handy-internal.com/engineering/developer-toolkit) -- run through the README. Make sure you've got your gitlab and github ssh keys setup, your AWS keys, as well as Duo authenticator. You might need to run the setup script a couple times

After running the setup script in the developer toolkit, you'll have access to the `hdy` command, which makes running some things on the command line pretty easy. The commandline help is a little out of date, but you can see a list of commands [here](https://gitlab.shared.handy-internal.com/engineering/developer-toolkit/blob/master/bin/hdy)

Some standouts are (ty @meverett):

```
# local console for handybook (use tab-complete to discover other services)
hdy run handybook-dev-env bundle exec rails c

# for sinatra services
hdy run service-pricing-backend-dev-env bundle exec irb -r ./app.rb

# or
hdy run service-pricing-backend-dev-env bash
bundle exec irb -r ./app.rb

# Namespace console
hdy namespace-console <namespace> <service>

# Prod read only console
# Ex: hdy prod-readonly-console leads
hdy prod-readonly-console <service-name>
```

#### Repos to explore

[handybook](https://gitlab.shared.handy-internal.com/engineering/handybook) -- the monolith that started it all. Runs a lot of handy.com, as well as provides some of the backend for mobile apps

#### Logging

[kibana for production](https://kibana-production.mon.handy-internal.com/app/kibana) -- our kibana frontend to elasticsearch logs

[handybook production error logs](https://hdy.io/h/4EdWne) -- here you can see we're filtering by the type of logger

[handybook Logger types](https://gitlab.shared.handy-internal.com/engineering/handybook/blob/master/app/loggers/loggers.rb) -- registered loggers used in handybook

[namespace logger](https://hdy.io/h/zjPQQN) -- logs for a namespace that you've spun up (see below for deets on namespaces)

#### Slack tools: Namespaces, deploys, rake tasks

##### Namespaces

[Namespace deployment](https://docs.google.com/document/d/1AxretfIyY5m4VmPjc2p0PWkb4XHXI8HuuYsriW2PGwk/edit?ts=5c509c3c#heading=h.7ne2x2cujtov) -- If local development isn't cutting it, or you need to start to emulate a prod environment, you can spin up a "namespace", which is a set of services spun up based on a specific working branch of code. To deploy one, go to the channel *#hdy-slacker* and type the command `/hdy ns up`. You'll see a prompt, where you can fill in the details you need. If you need to spin up multiple services in the same namespace, you can manually name your namespace, and rerun the command for each service you need in the namespace.

[namespace-name.int.handy-internal.com](https://kroy-test.int.handy-internal.com/services/home_cleaning) -- hit this link once the services you need have been deployed (if your namespace has a frontend)

You can also point a dev build of the app at your namespace if you need to test API changes.

##### Deploys

[Deploy docs](https://docs.google.com/document/d/1AxretfIyY5m4VmPjc2p0PWkb4XHXI8HuuYsriW2PGwk/edit?ts=5c509c3c#heading=h.sb41cnbgcf9l) -- Deploys happen on a service-by-service basis, and are managed in slack. To join the deploy queue for a service, go to that service's deploy channel and type `/hdy q add`. Once it's your turn, you'll get notified, and you can merge your working branch into master in gitlab. That should kick off a build, tests, and deploy. You can watch the progress of your deploy via the links the bot provides you. Once your deploy is done, the bot will provide you with a button to click to finish the deploy.

You should monitor your deploy, as detailed here: https://docs.google.com/document/d/19om0H_ciYwLQFFGjzRHRF7YJJVWCNn51d8oFrBHJQeI/edit#

`/hdy q rm` removes you from the queue. For reverting info, see the deploy doc

##### Rake tasks

[Trusty rake docs](https://docs.google.com/document/d/1AxretfIyY5m4VmPjc2p0PWkb4XHXI8HuuYsriW2PGwk/edit?ts=5c509c3c#heading=h.7tzq964oavwq) -- fairly straightforward, but note that you shouldn't use quotes when entering the rake command in the prompt!!

#### ConfigParams

README: https://gitlab.shared.handy-internal.com/engineering/handybook/blob/master/config/dips/README.md

#### Prod db views

Ask @slim for acess to `reporting-sql.handy-internal.com` and `reporting-services.handy-internal.com`. You can connect to these read-only views via your favorite mysql client to poke around in the prod data at your leisure.

#### Sandbox

https://www.handy-sandbox.com/

https://dash.handy-sandbox.com/login -- for logging into dashman in the sandbox

https://checkout.services.handy-sandbox.com/ -- for checkout. Other services follow the pattern you'd expect

#### Gem Development

Our gems are hosted in github eg: https://github.com/Handybook/gem-internal-client-api

Create a new feature branch on the gem remote, and push your changes to it periodically.

To test the new gem features locally, point a service's gem file entry for the gem you're developing to your new feature branch:

```
  gem "gem-handy-internal-api", handygit: "Handybook/gem-handy-internal-api", branch: "my-cool-branch"
```

Once your gem development is done, and your changes are approved, merge into master and create a new release, or tag your merge commit

* As an example (**obviously you need to go to the new release page for your specific gem**): https://github.com/Handybook/gem-internal-client-api/releases/new
* Another option is tagging your commit manually: `git tag -a v9.0.0.2 <sha>` (see https://git-scm.com/book/en/v2/Git-Basics-Tagging) and then pushing to origin

Then, point your service gem files to the new tag.

#### Other useful links/commands

Manually logging into `hdy prod-readonly-console`

```
kubectl config use-context production
kubectl get pods -n console-readonly
kubectl exec -ti <pod name> -n console-readonly bash
```

[JIRA](https://handybook.atlassian.net/secure/Dashboard.jspa)

[hdy.io link shortener](https://hdy.io/shorten)

[Architecture Proposals](https://drive.google.com/drive/folders/0B8IzvTNNQV8vTnpiYWRFSElrLW8)

[Rails guides handybook version (4.1.16)](https://guides.rubyonrails.org/v4.1/)
