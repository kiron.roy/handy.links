COMBINING_DIACRITICS_ABOVE = [0x036D, 0x0343, 0x0313, 0x0307, 0x030C, 0x0344, 0x0360, 0x0361, 0x035D]
COMBINING_DIACRITICS_BELOW = [0x034D, 0x035C, 0x035F, 0x0356, 0x0362, 0x033C]

def code_points_for(string:)
  #could also use string.codepoints
  string.unpack("U*").map { |code_point| code_point.to_s(16) }
end

def bits_in(string:)
  string.bytes.map { |b| b.to_s(2) }
end

def pack_unicode_codepoints(*code_points)
  code_points.pack("U*")
end

def zalgo(string:)
  code_points = []
  string.each_char do |c|
    code_points += c.codepoints
    rand(6..10).times { code_points << COMBINING_DIACRITICS_ABOVE.sample }
    rand(6..10).times { code_points << COMBINING_DIACRITICS_BELOW.sample }
  end
  s = pack_unicode_codepoints(*code_points)
  puts "\n\n\n\n\n"
  puts s
  puts "\n\n\n\n\n"
  return nil
end