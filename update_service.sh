#!/bin/bash
if ! [ -d ${HANDYDEV_ROOT}/repos/service-$1-backend ]; then \
      git clone git@gitlab.shared.handy-internal.com:engineering/service-$1-backend.git \
      ${HANDYDEV_ROOT}/repos/service-$1-backend/ ; fi

    cd ${HANDYDEV_ROOT}/repos/service-$1-backend
    echo $(pwd)
    git checkout master
    git checkout -- .
    git pull
    git checkout -b $2
    # run the function above to make the desired changes
    sed -i '' 's/gem "handy_logger".*$/gem "handy_logger", handylab: "gem-logger", tag: "v4.2.4"/' Gemfile
    hdy run service-$1-backend-dev-env bundle

    # docker-compose \
    #   --project-name handydev \
    #   -f ${COMPOSE_FILES_ROOT}/service-$svc-backend.yml \
    #   -f ${COMPOSE_FILES_ROOT}/mysql.yml \
    #   -f ${COMPOSE_FILES_ROOT}/redis.yml \
    #   -f ${COMPOSE_FILES_ROOT}/fluentd.yml \
    #   -f ${COMPOSE_FILES_ROOT}/gem-percoconut.yml \
    #   -f ${COMPOSE_FILES_ROOT}/gem-logger.yml \
    #   -f ${COMPOSE_FILES_ROOT}/kafka_writer.yml \
    #   -f ${COMPOSE_FILES_ROOT}/schematic_data.yml \
    #   -f ${COMPOSE_FILES_ROOT}/kafka.yml \
    #   -f ${COMPOSE_FILES_ROOT}/schema-registry.yml \
    #   -f ${COMPOSE_FILES_ROOT}/gem-internal-client-api.yml \
    #   -f ${COMPOSE_FILES_ROOT}/gem-partners-client.yml \
    #   -f ${COMPOSE_FILES_ROOT}/pub-sub.yml \
    #   -f ${COMPOSE_FILES_ROOT}/pim-engine.yml \
    #   -f ${COMPOSE_FILES_ROOT}/jwt-decode.yml \
    #   -f ${COMPOSE_FILES_ROOT}/svccompaniesapi-client.yml \
    #   -f ${COMPOSE_FILES_ROOT}/idempotent-keys.yml \
    #   -f ${COMPOSE_FILES_ROOT}/service-callerroutes-engine.yml \
    #   -f ${COMPOSE_FILES_ROOT}/network.yml \
    #   -f ${COMPOSE_FILES_ROOT}/${HANDYDEV_VOLUMES_FILE} \
    #   run service-$svc-backend-dev-env bundle

    git add Gemfile Gemfile.lock
    git commit -m "$3"
    git push origin $2
    glab mr create --no-editor \
      --target-branch master \
      --title "$3" \
      --description "$3" |grep https >> ~/$2-merge-requests.txt