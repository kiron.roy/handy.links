### GTM spec review

* Just 1:1 replacing signal/thebrighttag w/ GTM tags in head and body
* Crucial that we don't leak PII:
  * PII could be leaked by exposing specific URLs
  * or by specifically including PII in the data sent to GTM
  * **Needs to be centralized logic for this**
* Server-side rendered vs client: what are the different approaches we need?
* Spike to estimate level of effort for migrating non-pro acquisition funnel tags to GTM
* *Make sure we remove signal tags*

#### Questions

* Do we need the ability to add this on any page?
* What do we need to do to fire a new/specific event?
* GTM test acct? login w/ Handy SSO/ask hogan

#### Actions

* Get access to Handy GTM/other marketing platforms from Hogan
* Get list of what's considered PII from Chris
* Figure out current Signal implementation
* 
