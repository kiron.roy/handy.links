require 'net/http'
require 'uri'
require 'json'

uri = URI.parse("https://hdy-services.shared.handy-internal.com/enabled")
response = Net::HTTP.get_response uri
services = JSON.parse(response.body)

branch_name = "kr-g-logger-3"
commit_message = "updating our logger"
services = ["checkout", "partners"]
services.each do |svc|
  system("./update_service.sh #{svc} #{branch_name} '#{commit_message}'")
end